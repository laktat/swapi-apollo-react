import { AppProps } from 'next/app';
import { ApolloProvider } from '@apollo/client';

import './_app.scss';

import { useApollo } from '../lib/apollo';

const App = ({ Component, pageProps }: AppProps) => {
  const apolloClient = useApollo(pageProps);

  return (
    <ApolloProvider client={apolloClient}>
      <Component {...pageProps} />
    </ApolloProvider>
  );
};

export default App;
