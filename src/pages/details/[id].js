import { NextPage } from 'next';
import React, { useState } from 'react';
import { useApolloClient, gql, useQuery } from '@apollo/client';

import { useRouter } from 'next/router'

import Link from 'next/link'
import Button from 'react-bootstrap/Button';
import Pagination from 'react-bootstrap/Pagination';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

const ALL_CHARS_QUERY = gql`
  query Person($id: ID!) {
    person(id: $id) {
        name
        height
        mass
        hair_color
        skin_color
        eye_color
        gender
        url
        films {
            title
        }
    }
  }
`;

export default function Details() {
    const router = useRouter()
    
    let id = 1
    if( router != null ) {
        id = router.query.id
    }
    
    const { loading, error, data } = useQuery(ALL_CHARS_QUERY, {variables: { id: id }});

    if (error) {
        return <div>Error loading character.</div>;
    } 
    if (loading) {
        return null;
    }
    
    const { person: result } = data;
    
    return (
        <Container className="p-4">
            <Row>
                <Col><h1>Star Wars API</h1></Col>  
            </Row>

            <Row>
                <Col>
                    <Card>
                        <Card.Img variant="top" src={"http://placekitten.com/400/100" } />
                            <Card.Body>
                            <Card.Title>{result.name}</Card.Title>
                            <p>height: {result.height}cm<br/>
                                mass: {result.mass}kg<br/>
                                hair color: {result.hair_color}<br/>
                                skin color: {result.skin_color}<br/>
                                eye color: {result.eye_color}<br/>
                                gender: {result.gender}
                            </p>

                            <strong>appearances</strong>
                            <ul>
                            {result.films.map((film) => {
                                return <li>{film.title}</li>
                            })}
                            </ul> 
                            <Link href="/">
                                <a href="/" className="btn btn-primary" rel="noopener noreferrer" >back</a>
                            </Link>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
        
}