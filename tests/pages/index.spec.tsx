import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import { MockedProvider as ApolloMockedProvider } from '@apollo/client/testing';
import { createMocks } from 'node-mocks-http';

import Index from '../../src/pages/index';
import Detail from '../../src/pages/details/[id]';

it('Render index page', async () => {
  render(
    <ApolloMockedProvider>
      <Index />
    </ApolloMockedProvider>,
  );

  expect(screen.getByText(/Star Wars/i)).toBeInTheDocument();
});

describe('/details/[id]', () => {
  test('get char', async () => {
    const { req, res } = createMocks({
      method: 'GET',
      query: {
        id: '1',
      },
    });

    expect(res._getStatusCode()).toBe(200);
    expect.objectContaining({
      message: 'Luke Skywalker',
    })
  });
});
