import { NextPage } from 'next';
import React, { useState } from 'react';
import { gql, useQuery } from '@apollo/client';

import Link from 'next/link'
import Button from 'react-bootstrap/Button';
import Pagination from 'react-bootstrap/Pagination';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';
import Modal from 'react-bootstrap/Modal';
import Card from 'react-bootstrap/Card';

const ALL_CHARS_QUERY = gql`
  query People($search: String, $page: String) {
    people(search: $search, page: $page) {
      count
      next
      previous
      results {
        name
        height
        mass
        hair_color
        skin_color
        eye_color
        gender
        url
        films {
          title
        }
      }
    }
  }
`;

const IndexPage: NextPage = () => {
  
  const [pageCount, setPageCount] = useState(1);
  const [activePage, setActivePage] = useState(1);
  
  const { loading, error, data, fetchMore } = useQuery(ALL_CHARS_QUERY, {variables: { search: '', page: activePage }});
  
  
  return (
    <Container className="p-4">
      
      <Row>
        <Col><h1>Star Wars API</h1></Col>  
      </Row>

      <Row>
        {data ? (
            data.people.results.map((item: any) => {

              return (
                <Col lg={3} className="mb-4">
                  <Card>
                    { /* todo: find images */ }
                    <Card.Img variant="top" src={"http://placekitten.com/100/100" } />
                    <Card.Body>
                      <Card.Title>{item.name}</Card.Title>
                      <p>height: {item.height}cm<br/>
                        mass: {item.mass}kg<br/>
                        hair color: {item.hair_color}<br/>
                        skin color: {item.skin_color}<br/>
                        eye color: {item.eye_color}<br/>
                        gender: {item.gender}
                      </p>
                      
                      <Link href={"/details/" + item.url.split('/')[5] }>
                        <a className="btn btn-primary">details</a>
                      </Link>
                    </Card.Body>
                  </Card>
                </Col>
              )
                
            })
        ) : null}
        
      </Row>
    
      <Row>
        <Col>
          <Button
            disabled={ loading }
            onClick={ async () => {
              
              setActivePage( activePage + 1 )
              await fetchMore({
                updateQuery: (previousResult, { fetchMoreResult }) => {

                  const newResult = fetchMoreResult.people;
                  if (!newResult) return previousResult
                  return {
                    ...previousResult,
                    entry: {
                      ...previousResult,
                      ...newResult
                    },
                  };
                },
              });
            }}
          >
            Load More
          </Button>
        </Col>
      </Row>
    
      <Modal show={loading} onHide={ () => { return false }} >
        <Spinner className="spinner"  animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      </Modal>
    </Container>
  );
};    

export default IndexPage;