## Getting started

### install node modules

```bash
npm install
```

### Dev

```bash
npm start dev
```

### Tests

```bash
npm test
```

## Docker
To build and run Dockerized container, run:

```bash
docker-compose up --build
```
